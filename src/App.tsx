import * as React from 'react';
import './App.css';
import { BattleGrid } from './components/BattleGrid/BattleGrid'

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <h2>SEA BATTLE</h2>
        <BattleGrid
          gridScale={10}
          ships={{
            I: 2,
            L: 1,
            dot: 2
          }}
        />
      </div>
    );
  }
}

export default App;
