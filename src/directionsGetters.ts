type TGetAllDirections = ([[ R, C ]]: number[][]) => number[][][];

const getIShipDirections: TGetAllDirections = ([[ R, C ]])=> ([
  [[ R, C ], [ R + 1, C ], [ R + 2, C ], [ R + 3, C ]],
  [[ R, C ], [ R - 1, C ], [ R - 2, C ], [ R - 3, C ]],
  [[ R, C ], [ R, C + 1 ], [ R, C + 2 ], [ R, C + 3 ]],
  [[ R, C ], [ R, C - 1 ], [ R, C - 2 ], [ R, C - 3 ]]
]);

const getLShipDirections: TGetAllDirections = ([[ R, C ]])=> ([
  [[ R, C ], [ R + 1, C ], [ R + 2, C ], [ R + 2, C - 1 ]],
  [[ R, C ], [ R + 1, C ], [ R + 2, C ], [ R + 2, C + 1 ]],
  [[ R, C ], [ R - 1, C ], [ R - 2, C ], [ R - 2, C - 1 ]],
  [[ R, C ], [ R - 1, C ], [ R - 2, C ], [ R - 2, C + 1 ]],
  [[ R, C ], [ R, C + 1 ], [ R, C + 2 ], [ R - 1, C + 2 ]],
  [[ R, C ], [ R, C + 1 ], [ R, C + 2 ], [ R + 1, C + 2 ]],
  [[ R, C ], [ R, C - 1 ], [ R, C - 2 ], [ R - 1, C - 2 ]],
  [[ R, C ], [ R, C - 1 ], [ R, C - 2 ], [ R + 1, C - 2 ]]
]);

interface IDirectionsGetters {
  I: TGetAllDirections,
  L: TGetAllDirections,
}

export const directionsGetters: IDirectionsGetters = {
  I: getIShipDirections,  
  L: getLShipDirections,  
}
