import {differenceWith, flatten, isEqual, uniqWith} from 'lodash';
import * as React from 'react';
import { directionsGetters } from '../../directionsGetters';
import { getRandomNumberInRange } from '../../helpers'
import { Cell } from '../Cell/Cell';
import './BattleGrid.css';

interface IProps {
  gridScale: number,
  ships: {
    I: number,    
    L: number,
    dot: number
  }
}

interface IState {
  field: number[][],
  hasGameStarted: boolean,
  hasGaveUp: boolean
}

interface ICellStatus {
  default: number,
  missed: number,  
  reserved: number,
}

interface IShipStyles {
  damaged: number,  
  regular: number,
  sunk: number,
}

interface IMountedShipsCoordinates {
  dot: {
    coordinates: number[][][],
  },
  I: {
    coordinates: number[][][],
  },
  L: {
    coordinates: number[][][],
  }
}

type TGetCellsAroundCell = ([ R, C ]: number[][]) => number[][];

export class BattleGrid extends React.PureComponent<IProps, IState> {
  public mountedShipsCoordinates: IMountedShipsCoordinates = {
    I: {
      coordinates:[],
    },
    L: {
      coordinates:[],
    },
    dot: {
      coordinates: [],      
    }
  };

  public emptyCellStatus: ICellStatus = {
    default: 0,
    missed: 2,    
    reserved: 1,
  }

  public shipCellStatus: IShipStyles = {
    damaged: 4,    
    regular: 3,
    sunk: 5,
  }

  public state: IState = {
    field: this.getNewField(this.props.gridScale),
    hasGameStarted: false,
    hasGaveUp: false
  }

  public componentDidUpdate() {
    const areAllShipsSunked = !this.state.field.some(row => row.some(cell => cell === this.shipCellStatus.regular));
    
    if (this.state.hasGameStarted && areAllShipsSunked) {
      setTimeout(() => {
        alert('Congratulations! You have won!');

        this.setState({hasGameStarted: false});                        
        this.setState({field: this.getNewField(this.props.gridScale)});
      }, 0);
    }
  }
  
  public getNewField(fieldSize: number): number[][] {
    return Array(fieldSize).fill(Array(fieldSize).fill(this.emptyCellStatus.default));    
  }

  public pickARandomCell = (field: number[][]): number[][] => {
    const rowCoordinate = getRandomNumberInRange(0, field.length - 1);
    const columnCoordinate = getRandomNumberInRange(0, field.length - 1);
    const isCellDefault = field[rowCoordinate][columnCoordinate] === 0;

    if (isCellDefault) {
      return [[rowCoordinate, columnCoordinate]];
    }
    else {
      return this.pickARandomCell(field);
    }
  }
  
  public getCellsAroundCell: TGetCellsAroundCell = ([[ R, C ]])=> ([
    [ R, C - 1], [ R, C + 1 ],
    [ R - 1, C], [ R + 1, C ],
    [ R - 1, C - 1 ], [ R + 1, C + 1],
    [ R + 1, C - 1 ], [ R - 1, C + 1]
  ]);

  public placeCellsOnField = (cellsToPlace: number[][], field: number[][], cellStyle: number): number[][] => {
    return cellsToPlace.reduce((acc, curr, index) => {
      return acc.map((row, rowIndex) => {
        if (curr[0] === rowIndex) {
          return row.map((column, columnIndex) => {
            if (curr[1] === columnIndex) {
              return cellStyle;
            }
            else {
              return column;
            }
          })
        }
        else {
          return row;
        }
      })
    }, field);
  }

  public filterInRange = (cellsToFilter: number[][], field: number[][]): number[][] => {
    return cellsToFilter
      .filter(cellToFilterInRange => {
        const rowCoordinate = cellToFilterInRange[0];
        const columnCoordinate = cellToFilterInRange[1];
        const areCoordiantesInRange = 
          rowCoordinate >= 0 && rowCoordinate <= field.length - 1
          && columnCoordinate >= 0 && columnCoordinate <= field.length - 1;

        return areCoordiantesInRange;
      })
  }

  public filterCells = (cellsToFilter: number[][], field: number[][]): number[][] => {
    return this.filterInRange(cellsToFilter, field)
      .filter(cellToFilterOccupied => {
        const rowCoordinate = cellToFilterOccupied[0];
        const columnCoordinate = cellToFilterOccupied[1];
        const areCoordinatesNotOccupied = field[rowCoordinate][columnCoordinate] !== this.emptyCellStatus.reserved;
        
        return areCoordinatesNotOccupied;
    })
  }

  public getFieldWithPlacedDotShips = (field: number[][], countOfShipsToMount: number): number[][] => {
    if (countOfShipsToMount > 0) {
      const shipCell = this.pickARandomCell(field);
      this.mountedShipsCoordinates.dot.coordinates = [...this.mountedShipsCoordinates.dot.coordinates, shipCell];    
      const fieldWithShipCell = this.placeCellsOnField(shipCell, field, this.shipCellStatus.regular);

      const cellsAroundRandomCell = this.getCellsAroundCell(shipCell);
      const occupiedCells = this.filterCells(cellsAroundRandomCell, field);
      const fieldWithOccupiedCells = this.placeCellsOnField(occupiedCells, fieldWithShipCell, this.emptyCellStatus.reserved);      

      return this.getFieldWithPlacedDotShips(fieldWithOccupiedCells, countOfShipsToMount - 1);
    }
    else {
      return field;
    }
  }

  public pickADirection = (directions: number[][][], field: number[][]): number[][] | undefined => {
    const hasDirections = directions.length > 0;

    if (hasDirections) {
      const directionToPick = getRandomNumberInRange(0, directions.length - 1);
      const pickedDirection = directions[directionToPick];
      const validLength = pickedDirection.length;
      const filteredCells = this.filterCells(pickedDirection, field);
      const isValidDirection = filteredCells.length === validLength;

      if (isValidDirection) {
        return filteredCells;
      } else {
        const removeInvalidDirection = (direction: number[][], index: number) => index !== directionToPick;

        return this.pickADirection(directions.filter(removeInvalidDirection), field);
      }
    } else {
      return undefined;
    }
  }

  public getShipCells = (field: number[][], coordinatesGetter: ([[ R, C ]]: number[][]) => number[][][]): number[][] => {
    const possibleDirections = coordinatesGetter(this.pickARandomCell(field));
    const pickedDireciton = this.pickADirection(possibleDirections, field);
    const isDirectionDefined = pickedDireciton !== undefined;    

    if (isDirectionDefined) {
      return pickedDireciton;
    } else {
      return this.getShipCells(field, coordinatesGetter);      
    }
  }

  public getFieldWithPlacedComplexShips = (field: number[][], countOfShipsToMount: number, directionsGetter: ([[ R, C ]]: number[][]) => number[][][], shipType: string): number[][] => {
    if (countOfShipsToMount > 0) {
      const shipCells = this.getShipCells(field, directionsGetter);

      this.mountedShipsCoordinates[shipType].coordinates = [...this.mountedShipsCoordinates[shipType].coordinates, shipCells];    

      const fieldWithShipCells = this.placeCellsOnField(shipCells, field, this.shipCellStatus.regular);

      const cellsAroundShipCell = shipCells.map(shipCell => this.getCellsAroundCell([shipCell]));
      const filteredCellsAroundShipCells = cellsAroundShipCell.map(cellAroundShipCell => this.filterCells(cellAroundShipCell, fieldWithShipCells));
      const uniqFilteredCellsAroundShipCells = uniqWith(flatten(filteredCellsAroundShipCells), isEqual);
      const occupiedCells = differenceWith(uniqFilteredCellsAroundShipCells, shipCells, isEqual);
      const fieldWithOccupiedCells = this.placeCellsOnField(occupiedCells, fieldWithShipCells, this.emptyCellStatus.reserved);      

      return this.getFieldWithPlacedComplexShips(fieldWithOccupiedCells, countOfShipsToMount - 1, directionsGetter, shipType);
    }
    else {
      return field;
    }
  }

  public mountEnemyShips = (field: number[][] = this.state.field) => () => {
    this.setState({hasGameStarted: true})

    const { dot, I, L } = this.props.ships;
    
    const fieldWithPlacedDotShips = this.getFieldWithPlacedDotShips(field, dot);
    const fieldWithPlacedIShips = this.getFieldWithPlacedComplexShips(fieldWithPlacedDotShips, I, directionsGetters.I, 'I');
    const fieldWithPlacedLShips = this.getFieldWithPlacedComplexShips(fieldWithPlacedIShips, L, directionsGetters.L, 'L');    
    
    this.setState({field: fieldWithPlacedLShips})
  }

  public remountEnemyShips = (): void => {
    this.setState({hasGaveUp: false})    
    const newField = this.getNewField(this.props.gridScale);

    this.mountEnemyShips(newField)();
  }

  public getShot = (coordinates: {row: number, column: number}): void => {
    const {field} = this.state;
    const {row, column} = coordinates;
    const shot = this.state.field[row][column];
    const setShotInState = (shotTry: number): void => {
      const newField = this.placeCellsOnField([[row, column]], [...field], shotTry);
      this.setState({field: newField});
    };

    switch(shot) {
      case 0:
      case 1:
        setShotInState(this.emptyCellStatus.missed);
        break;
      case 3:
        const fieldCopy: number[][] = this.state.field;
        fieldCopy[row][column] = this.shipCellStatus.damaged;        

        const shipCoordinates = Object.keys(this.mountedShipsCoordinates).reduce((acc, curr) => {
          acc = [...acc, ...this.mountedShipsCoordinates[curr].coordinates];

          return acc;
        }, []);

        const sunkShip = shipCoordinates.reduce((acc, curr) => {
          const shipCellsStatuses = curr.map((coord: number[]) => fieldCopy[coord[0]][coord[1]]);
          const isShipSunk = shipCellsStatuses.every((shipCellStatus: number) => shipCellStatus === this.shipCellStatus.damaged);

          if (isShipSunk) {
            return curr;
          }
          else {
            return acc;
          }
        }, undefined);

        if (sunkShip) {
          const fieldWithSunkShip = this.placeCellsOnField(sunkShip, fieldCopy, this.shipCellStatus.sunk);
          const occupiedCells = sunkShip.map((sunkShipCell: any) => this.getCellsAroundCell([sunkShipCell]));
          const filteredOccupiedCells = differenceWith(this.filterInRange(flatten(occupiedCells), fieldWithSunkShip), sunkShip, isEqual); 
          const newFieldWithSunkShipAndOccupiedCells = this.placeCellsOnField(filteredOccupiedCells, fieldWithSunkShip, this.emptyCellStatus.missed);

          this.setState({field: newFieldWithSunkShipAndOccupiedCells});
        }
        else {
          setShotInState(this.shipCellStatus.damaged);
        }
        break;
      case 4:
      case 5:
      case 2:      
        break;
    }
  }

  public render() {
    const { field, hasGameStarted } = this.state;

    return (
      <div className='container'>
        <div className='navigation'>
          {hasGameStarted
           ? 
            <a className='restart-game' onClick={this.remountEnemyShips}>
              Restart game
            </a>
          :
           <a className='start-game' onClick={this.mountEnemyShips()}>
              Start game
            </a>
          }
        </div>
        <div className='battle-field'>
          {field.map((row, rowIndex) => (
            <div key={rowIndex} className='row'>
              {row.map((cell, cellIndex) => (
                <span key={cellIndex} className='column'>
                  <Cell
                    status={field[rowIndex][cellIndex]}
                    position={{
                      column: cellIndex,
                      row: rowIndex,
                    }}
                    passShot={hasGameStarted ? this.getShot : () => null}
                    hasGameStarted={hasGameStarted}
                  />
                </span>
              ))}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
