import * as React from 'react';
import './Cell.css'

interface IProps {
  status: number,
  position: {
    row: number,
    column: number
  },
  hasGameStarted: boolean,  
  passShot(coordinates: {row: number, column: number}): void,
}

interface IState {
  activeStatus: number
}

export class Cell extends React.PureComponent<IProps, IState> {
  protected static getDerivedStateFromProps(nextProps: IProps, prevState: IState): IState {
    if (nextProps.status === prevState.activeStatus) {
      return null
    }

    return {
      activeStatus: nextProps.status
    }
  }  
  
  public state: IState = {
    activeStatus: 0    
  }

  public getCellStatusClass(activeStatus: number): string {
    switch(activeStatus) {
      case 1:
        return 'cell-occupied';
      case 2:
        return 'cell-missed';
      case 3:
        return 'ship-regular';
      case 4:
        return 'ship-damaged';
      case 5:
        return 'ship-sunk';
      case 0:        
      default:
        return 'cell-default'
    }
  }

  public handleOnClick = (position: {row: number, column: number}) => () => {
    this.props.passShot(position)
  }
  
  public render() {
    const {position, status, hasGameStarted} = this.props;

    return (
      <div
        id={`${position.row}-${position.column}`}
        className={`cell-unit ${this.getCellStatusClass(this.state.activeStatus)} ${hasGameStarted && (status === 0 || status === 1 || status === 3) ? 'aim' : ''}`.trim()}
        onClick={this.handleOnClick(position)}
      />
    )
  }
}